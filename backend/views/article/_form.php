<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\component\Ueditor;

/* @var $this yii\web\View */
/* @var $model common\models\OmsArticle */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="oms-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    //设置表单中作者的值
    if ($model->author) {
        $value = $model->author;
    } else {
        $value = \Yii::$app->user->identity->login_account;
    }
    ?>
    <div class="row">
        <div class="form-group field-omsarticle-title required col-md-5">
            <label for="omsarticle-title" class="control-label">标题</label>
            <input type="text" maxlength="255" value="<?= $model->title ?>" name="OmsArticle[title]" class="form-control" id="omsarticle-title">

            <div class="help-block"></div>
        </div>

        <div class="form-group field-omsarticle-add_time col-md-2">
            <label for="omsarticle-add_time" class="control-label">添加时间</label>
            <input type="text" readonly="" value="<?= $model->add_time ?>" name="OmsArticle[add_time]" class="form-control" id="omsarticle-add_time">

            <div class="help-block"></div>
        </div>

        <div class="form-group field-omsarticle-author col-md-2">
            <label for="omsarticle-author" class="control-label">作者</label>
            <input type="text" maxlength="255" readonly="" value="<?= $value ?>" name="OmsArticle[author]" class="form-control" id="omsarticle-author">

            <div class="help-block"></div>
        </div>

        <?php if (isset($category)) { ?>
            <div class="form-group field-omsarticle-type_id col-md-1">
                <label for="omsarticle-type_id" class="control-label">分类：</label>
                <select name='OmsArticle[type_id]' id="omsarticle-type_id">
                    <?php foreach ($category as $v) { ?>
                        <option value="<?= $v['id'] ?>" <?php if(isset($model->type_id) && $model->type_id == $v['id']){echo "selected='selected'";}?>><?= $v['type_name'] ?></option>
                    <?php } ?>
                </select>
                <div class="help-block"></div>
            </div>
        <?php } ?>
        <?php //$form->field($model, 'status')->textInput() ?>
        <div class="form-group field-omsarticle-status col-md-1">
            <label for="omsarticle-status" class="control-label">状态：</label>
            <select name='OmsArticle[status]' id="omsarticle-status">
                <option value="0" <?php if(!isset($model->status) || $model->status == 0){echo "selected='selected'";}?>>未审核</option>
                <option value="1" <?php if(isset($model->status) && $model->status == 1){echo "selected='selected'";}?>>审核</option>
            </select>
            <div class="help-block"></div>
        </div>

        <div class="form-group col-md-1">
            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="form-group field-smhelp-content required">
        <label for="smhelp-content" class="control-label">内容</label>
        <?php
        echo Ueditor::widget([
            'name' => 'OmsArticle[content]',
            'options' => [
                'id' => 'OmsArticle-content',
                'initialContent' => isset($model->content) ? htmlspecialchars_decode($model->content) : '',
                'focus' => true,
                'toolbars' => [
                    [
                        'fullscreen', 'source', 'undo', 'redo', '|',
                        'fontsize',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat',
                        'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
                        'forecolor', 'backcolor', '|',
                        'lineheight', '|',
                        'indent', '|',
                        'link', '|',
                        'simpleupload', '|', 'insertimage', 'insertvideo'
                    ],
                ],
            ],
            'attributes' => [
                'style'=>'height:400px;width:100%;'
            ]
        ]);
        ?>
        <div class="help-block"></div>
    </div>
    <?php ActiveForm::end(); ?>

</div>