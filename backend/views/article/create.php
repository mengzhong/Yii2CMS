<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OmsArticle */

$this->title = '发布文章';
$this->params['breadcrumbs'][] = ['label' => 'Oms Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oms-article-create" style="margin-left: 1%;margin-right:1%;">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'category' => $category,
    ]) ?>

</div>
