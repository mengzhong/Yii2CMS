<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OmsArticle */

$this->title = '更新文章: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Oms Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="oms-article-update" style="margin-left: 1%;margin-right:1%;">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'category' => $category,
    ]) ?>

</div>
