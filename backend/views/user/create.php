<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SysUser */

$this->title = '添加用户';
$this->params['breadcrumbs'][] = ['label' => 'Sys Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sys-user-create" style="width: 70%;margin-left: 1%;">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
