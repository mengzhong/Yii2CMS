<?php
$params = require(__DIR__ . '/params.php');

$config = [
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','gii'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
        'gii' => 'yii\gii\Module'
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'zh-CN',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'test' => 'site/test',
                'log/model'=>'log/model-log',
                'log/sys' => 'log/sys-log',
                'log/user' => 'log/user-log',
                'cache' => 'sys-cache/init-cache',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
    ],
//     'errorHandler' => [
//         'errorAction' => 'site/error',
//     ],
//     'mailer' => [
//         'class' => 'yii\swiftmailer\Mailer',
//         'transport' => [
//             'class' => 'Swift_SmtpTransport',
//             'host' => 'smtp.qq.com',
//             'username' => 'staoke@126.com  ',
//             'password' => 's123456789',
//             'port' => '465',
//             'encryption' => 'ssl',
//         ],
//     ],
];
$db = require(__DIR__ . '/db.php');
// $cache = require(__DIR__ . '/cache.php');
// $config =  yii\helpers\ArrayHelper::merge($config, $db, $cache);
$config =  yii\helpers\ArrayHelper::merge($config, $db);
return $config;