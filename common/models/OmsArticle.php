<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%oms_article}}".
 *
 * @property string $id
 * @property string $title
 * @property string $content
 * @property string $add_time
 * @property string $author
 * @property integer $type_id
 * @property integer $status
 */
class OmsArticle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%oms_article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['add_time'], 'safe'],
            [['type_id', 'status'], 'integer'],
            [['title', 'author'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'content' => '内容',
            'add_time' => '添加时间',
            'author' => '作者',
            'type_id' => '分类ID',
            'status' => '状态',
        ];
    }
}
