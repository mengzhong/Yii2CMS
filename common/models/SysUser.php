<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%sys_user}}".
 *
 * @property string $id
 * @property string $login_account
 * @property string $user_name
 * @property string $password
 * @property string $email
 * @property integer $qq
 * @property string $last_ip
 * @property string $last_login_time
 * @property string $reg_time
 * @property integer $sex
 * @property integer $status
 * @property string $icon
 */
class SysUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sys_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login_account', 'user_name', 'password'], 'required'],
            [['qq', 'sex', 'status'], 'integer'],
            [['last_login_time', 'reg_time'], 'safe'],
            [['login_account', 'user_name', 'icon'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 128],
            [['last_ip'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login_account' => '登录帐号',
            'user_name' => '用户名',
            'password' => '密码',
            'email' => 'Email',
            'qq' => 'QQ',
            'last_ip' => '最后一次登录IP',
            'last_login_time' => '最后登录时间',
            'reg_time' => '注册时间',
            'sex' => '性别',
            'status' => '状态',
            'icon' => '头像地址',
        ];
    }
}
