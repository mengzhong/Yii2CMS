<aside class="col-md-4">
    <div class="widget search">
        <form role="form">
            <input type="text" class="form-control search_box" autocomplete="off" placeholder="Search Here">
        </form>
    </div><!--/.search-->

    <div class="widget categories">
        <h3>最新评论</h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="single_comments">
                    <img src="/images/blog/avatar3.png" alt=""  />
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
                    <div class="entry-meta small muted">
                        <span>By <a href="#">Alex</a></span <span>On <a href="#">Creative</a></span>
                    </div>
                </div>
                <div class="single_comments">
                    <img src="/images/blog/avatar3.png" alt=""  />
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
                    <div class="entry-meta small muted">
                        <span>By <a href="#">Alex</a></span <span>On <a href="#">Creative</a></span>
                    </div>
                </div>
                <div class="single_comments">
                    <img src="/images/blog/avatar3.png" alt=""  />
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do </p>
                    <div class="entry-meta small muted">
                        <span>By <a href="#">Alex</a></span <span>On <a href="#">Creative</a></span>
                    </div>
                </div>

            </div>
        </div>                     
    </div><!--/.recent comments-->


    <div class="widget categories">
        <h3>分类</h3>
        <div class="row">
            <div class="col-sm-6">
                <ul class="blog_category">
                    <?php if (!empty($category)) {
                        foreach ($category as $v) {
                            ?>
                            <li><a href="/article/index?category=<?= $v['id'] ?>"><?= $v['type_name'] ?> <span class="badge"><?= $v['num'] ?></span></a></li>
                    <?php }} ?>
                        </ul>
                    </div>
                </div>                     
            </div><!--/.categories-->

            <div class="widget archieve">
                <h3>文章归档</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="blog_archieve">
                            <?php
                            if (!empty($archieve)) {
                                foreach ($archieve as $v) {
                                    ?>
                                    <li><a href="/article/index?archieve=<?= $v['time'] ?>"><i class="fa fa-angle-double-right"></i> <?= $v['time'] ?> <span class="pull-right">(<?= $v['num'] ?>)</span></a></li>
                                <?php }
                            }
                            ?>
                </ul>
            </div>
        </div>                     
    </div><!--/.archieve-->

    <div class="widget tags">
        <h3>标签云 Tag Cloud</h3>
        <ul class="tag-cloud">
            <li><a class="btn btn-xs btn-primary" href="#">Apple</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Barcelona</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Office</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Ipod</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Stock</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Race</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">London</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Football</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Porche</a></li>
            <li><a class="btn btn-xs btn-primary" href="#">Gadgets</a></li>
        </ul>
    </div><!--/.tags-->

    <div class="widget blog_gallery">
        <h3>Our Gallery</h3>
        <ul class="sidebar-gallery">
            <li><a href="#"><img src="/images/blog/gallery1.png" alt="" /></a></li>
            <li><a href="#"><img src="/images/blog/gallery2.png" alt="" /></a></li>
            <li><a href="#"><img src="/images/blog/gallery3.png" alt="" /></a></li>
            <li><a href="#"><img src="/images/blog/gallery4.png" alt="" /></a></li>
            <li><a href="#"><img src="/images/blog/gallery5.png" alt="" /></a></li>
            <li><a href="#"><img src="/images/blog/gallery6.png" alt="" /></a></li>
        </ul>
    </div><!--/.blog_gallery-->
</aside>  
</div><!--/.row-->
</div>
</section><!--/#blog-->
