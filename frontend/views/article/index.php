<?php require 'header.php';?>
<div class="col-md-8">
    <?php
    if (!empty($article)) {
        foreach ($article as $v) {
            ?>
            <div class="blog-item">
                <div class="row">
                    <div class="col-sm-2 text-center">
                        <div class="entry-meta"> 
                            <span id="publish_date"><?= date("Y-m-d", strtotime($v['add_time'])) ?></span>
                            <span><i class="fa fa-user"></i> <a href="#"> <?= $v['author'] ?></a></span>
                            <span><i class="fa fa-comment"></i> <a href="blog-item.html#comments"> 2 评论</a></span>
                            <span><i class="fa fa-heart"></i><a href="#"> 56 赞</a></span>
                        </div>
                    </div>
                    <div class="col-sm-10 blog-content">
                        <!-- 显示文章图片 -->
                        <a href="" style="display: none;"><img class="img-responsive img-blog" src="/images/blog/blog2.jpg" width="100%" alt="" /></a>
                        <h2><a href="/article/blog?id=<?= $v['id'] ?>"><?= $v['title'] ?></a></h2>
                        <h3><?= t_sub(strip_tags($v['content']), 80) ?></h3>
                        <a class="btn btn-primary readmore" href="/article/blog?id=<?= $v['id'] ?>">更多 <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>    
            </div><!--/.blog-item-->
        <?php }
    }
    ?>

    <ul class="pagination pagination-lg">
        <li><a href="#"><i class="fa fa-long-arrow-left"></i>Previous Page</a></li>
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">Next Page<i class="fa fa-long-arrow-right"></i></a></li>
    </ul><!--/.pagination-->
</div><!--/.col-md-8-->
<?php require 'footer.php';?>