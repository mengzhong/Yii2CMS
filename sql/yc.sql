/*
Navicat MySQL Data Transfer

Source Server         : conn
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : yc

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-07-01 14:50:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for yc_auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `yc_auth_assignment`;
CREATE TABLE `yc_auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `created_at` (`created_at`),
  KEY `item_name` (`item_name`),
  CONSTRAINT `yc_auth_assignment_ibfk_2` FOREIGN KEY (`item_name`) REFERENCES `yc_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员授权表';

-- ----------------------------
-- Table structure for yc_auth_item
-- ----------------------------
DROP TABLE IF EXISTS `yc_auth_item`;
CREATE TABLE `yc_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  KEY `name` (`name`),
  KEY `created_at` (`created_at`),
  CONSTRAINT `yc_auth_item_ibfk_2` FOREIGN KEY (`rule_name`) REFERENCES `yc_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理权权限条目';

-- ----------------------------
-- Table structure for yc_auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `yc_auth_item_child`;
CREATE TABLE `yc_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限关系表';

-- ----------------------------
-- Table structure for yc_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `yc_auth_rule`;
CREATE TABLE `yc_auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `name` (`name`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限规则表';

-- ----------------------------
-- Table structure for yc_menu
-- ----------------------------
DROP TABLE IF EXISTS `yc_menu`;
CREATE TABLE `yc_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  KEY `name` (`name`),
  KEY `route` (`route`(255)),
  KEY `order` (`order`),
  CONSTRAINT `dh_menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `dh_menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统管理员菜单权限表\r\n';

-- ----------------------------
-- Table structure for yc_oms_article
-- ----------------------------
DROP TABLE IF EXISTS `yc_oms_article`;
CREATE TABLE `yc_oms_article` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `type_id` int(8) NOT NULL DEFAULT '0' COMMENT '分类ID',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '审核状态0未审核1审核通过',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for yc_sys_category
-- ----------------------------
DROP TABLE IF EXISTS `yc_sys_category`;
CREATE TABLE `yc_sys_category` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) NOT NULL COMMENT '分类名称',
  `parent_id` int(8) NOT NULL DEFAULT '0' COMMENT '父类ID',
  `sort` int(8) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for yc_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `yc_sys_user`;
CREATE TABLE `yc_sys_user` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `login_account` varchar(255) NOT NULL COMMENT '登录帐号',
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `qq` int(13) DEFAULT NULL COMMENT 'QQ号',
  `last_ip` varchar(16) DEFAULT NULL COMMENT '最后登录IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '注册时间',
  `reg_time` datetime DEFAULT NULL COMMENT '注册时间',
  `sex` int(2) DEFAULT NULL COMMENT '性别',
  `status` int(2) DEFAULT '0' COMMENT '审核状态0未审核1审核通过',
  `icon` varchar(255) DEFAULT 'default.png' COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
