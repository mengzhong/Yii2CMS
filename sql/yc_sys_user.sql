SET FOREIGN_KEY_CHECKS=0;
/**
 * 文章表
 */
-- ----------------------------
-- Table structure for yc_oms_article
-- ----------------------------
DROP TABLE IF EXISTS `yc_oms_article`;
CREATE TABLE `yc_oms_article` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `type_id` int(8) NOT NULL DEFAULT 0 COMMENT '分类ID',
  `status` int(2) NOT NULL DEFAULT 0 COMMENT '审核状态0未审核1审核通过',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
 * 用户表
 */
-- ----------------------------
-- Table structure for yc_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `yc_sys_user`;
CREATE TABLE `yc_sys_user` (
    `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
    `login_account` varchar(255) NOT NULL COMMENT '登录帐号',
    `user_name` varchar(255) NOT NULL COMMENT '用户名',
    `password` varchar(32) NOT NULL COMMENT '密码',
    `email` varchar(128) COMMENT '邮箱',
    `qq` int(13) COMMENT 'QQ号',
    `last_ip` varchar(16) COMMENT '最后登录IP',
    `last_login_time` datetime COMMENT '注册时间',
    `reg_time` datetime DEFAULT NULL COMMENT '注册时间',
    `sex` int(2) COMMENT '性别',
    `status` int(2) NOT NULL DEFAULT 0 COMMENT '审核状态0未审核1审核通过',
    `icon` varchar(255) DEFAULT 'default.png' COMMENT '头像',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
 * 分类表
 */
 DROP TABLE IF EXISTS `yc_sys_category`;
 CREATE TABLE `yc_sys_category` (
    `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
    `type_name` varchar(255) NOT NULL COMMENT '分类名称',
    `parent_id` int(8) NOT NULL DEFAULT 0 COMMENT '父类ID',
    `sort` int(8) NOT NULL DEFAULT 0 COMMENT '排序',
    PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
