<?php

/**
 * Message translations for \mdmsoft\yii2-admin.
 *
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Assignments' => '授权管理',
    'Users' => '用户管理',
    'User' => '用户',
    'Avaliable' => '待分配',
    'Assigned' => '已分配',
    'Create' => '添加',
    'Update' => '更新',
    'Roles' => '角色管理',
    'Create Role' => '创建角色',
    'Name' => '名称',
    'Type' => 'Type',
    'Description' => '描述',
    'Rule Name' => '规则名称',
    'Data' => 'Data',
    'Update Role' => '编辑角色',
    'Delete' => '删除',
    'Are you sure to delete this item?' => '你确定要删除这条信息吗?',
    'ID' => 'ID',
    'Parent' => '父级',
    'Parent Name' => '父级名称',
    'Route' => '路由地址',
    'Username' => '用户名',
    'Update Permission' => '更新权限',
    'Permissions' => '权限管理',
    'Permission' => '权限',
    'Create Permission' => '添加权限',
    'Create Permissions' => 'Create Permissions',
    'Routes' => '路由管理',
    'Create route' => '添加路由',
    'New' => 'New',
    'Generate Routes' => 'Generate Routes',
    'Append' => '附加',
    'Create Rule' => '添加规则',
    'Rules' => '规则管理',
    'Update Rule' => '更新规则',
    'Create Menu' => '添加菜单',
    'Menus' => '菜单',
    'Search' => '搜索',
    'Reset' => '重置',
    'Update Menu' => '更新菜单',
    'Menus' => '菜单管理',
    'Order' => '排序',
    'Class Name' => '类名',
    'Assignment' => '授权',
    'Role' => '角色',
    'Rule' => '规则',
    'Menu' => '菜单',
    'Help' => '帮主',
    'Application' => '应用',
];
